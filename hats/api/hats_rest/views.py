import json
from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
from traitlets import HasTraits
from hats.api.hats_rest.models import Hat

from wardrobe.api.wardrobe_api.models import Location
from wardrobe.api.wardrobe_api.views import LocationEncoder

@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_hat(request, pk):
    if request.method == "GET":
        
    #will need to return to show hats
        hats = Hat.objects.get(id=pk),
        locations = Location.objects.all(),
        return JsonResponse(
            {"locations": locations},
            encoder=LocationEncoder
        )
    else:
        content = json.loads(request.body)
@require_http_methods(["GET", "POST"])
def api_list_hats(request):
    if request.method == "GET":
        Hat = Hat.objects.all()
        return JsonResponse(
            {"hats": Hat},
            encoder=json.JSONEncoder
        )



        
    