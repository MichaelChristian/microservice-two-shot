from django.db import models
from django.urls import reverse

from wardrobe.api.wardrobe_api.models import Location





# from wardrobe.api.wardrobe_api.models import Location


# Create your models here.
class Hat(models.Model):
    fabric = models.TextField(),
    style = models.TextField(),
    color = models.CharField(max_length = 30),
    url = models.URLField(),
    location = models.OneToOneField(
        Location,
        related_name ="hats",
        on_delete=models.CASCADE
    ),

    def get_api_url(self):
        return reverse("api_show_hats", kwargs={"pk": self.pk})
    def __str__(self):
        return self.name

class Location_VO(models.Model):
    fabric = models.EmailField(unique=True)
    style = models.CharField(max_length=200)
    color = models.CharField(max_length=200)
    url = models.DateTimeField()