from django.urls import path
from views import (
    api_show_hat,
    api_list_hats
)

urlpatterns = [ 
    path("locations/", api_show_hat, name="api_url_showHats"),
    path("locations", api_list_hats, name="api_url_listHats" ),
]

