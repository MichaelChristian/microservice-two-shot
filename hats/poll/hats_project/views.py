from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder
from .models import Shoe, BinVO
# Create your views here.

class BinVODetailEncoder(ModelEncoder):
    model = BinVO
    properties = ['closet_name', 'bin_number', 'bin_size', 'import_href']

class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = ['model_name']

class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = ['manufacturer', 'model_name', 'color', 'shoe_url', 'bin']
    encoders = {'bin': BinVODetailEncoder}

@require_http_methods(['GET', 'POST'])
def api_list_shoes(request, bin_vo_id=None):
    if request.method == 'Get':
        if bin_vo_id is not None:
            shoes = Shoe.objects.filter(bin=bin_vo_id)
        else:
            shoes = Shoe.objects.all()
        return JsonResponse(
            {'shoes': shoes},
            encoder=ShoeListEncoder
            )
    else:
        content = json.loads(request.body)

        try:
            bin_href = content['bin']
            bin = BinVO.objects.get(import_href=bin_href)
            content['bin'] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {'message': 'Invalid bin id'},
                status=400
            )
        shoe = Shoe.objects.create(**content)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False
        )

@require_http_methods(['GET', 'PUT', 'DELETE'])
def api_detail_shoes(request, pk):
    try:
        shoe = Shoe.objects.get(id=pk)
    except Shoe.DoesNotExist:
        return JsonResponse({'message': 'Invalid Shoe ID'}, status=400)

    if request.method == 'GET':
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False
        )
    elif request.method == 'PUT':
        content = json.loads(request.body)
    else:
        pass
