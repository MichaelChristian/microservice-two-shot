import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "hats_project.settings")
django.setup()
from wardrobe.api.wardrobe_api.models import Location
from hats.api.hats_rest.models import Hat, fabric,style,color,url

# Import models from hats_rest, here.
# from hats_rest.models import Something

def poll():
    while True:
        print('Hats poller polling for data')
        try:
            # if poll == True:
            #     pass

            if Hat is True:
                get(fabric,style,color,url,Location)
            # list in hats.api.hats_rest.admin.py
            list(fabric,style,color,url,Location)
            #hopefully this will list where it needs to be
            # but pretty sure it will not.
            pass

        except Exception as e:
            print(e, file=sys.stderr)
        time.sleep(60)


if __name__ == "__main__":
    poll()
